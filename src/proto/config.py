import os

import dotenv

dotenv.load_dotenv()

MICRO_TEST_HOST = str(os.environ.get("MICRO_TEST_HOST", "localhost"))
MICRO_TEST_PORT = int(os.environ.get("MICRO_TEST_PORT", 5090))

if __name__ == "__main__":
    print(f"MICRO_TEST_HOST: {MICRO_TEST_HOST}")
    print(f"MICRO_TEST_PORT: {MICRO_TEST_PORT}")
