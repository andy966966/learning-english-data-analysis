import random

import base_pb2
import base_pb2_grpc
import config
import grpc
import test_pb2
import test_pb2_grpc
import user_pb2
import user_pb2_grpc
from compile_grpc import compile_protos


class TestClient(object):
    def __init__(self):
        self.host = config.MICRO_TEST_HOST
        self.port = config.MICRO_TEST_PORT

        print(f"self.host: {self.host}, self.port: {self.port}")

        # instantiate a channel
        self.channel = grpc.insecure_channel(f"{self.host}:{self.port}")

        # bind the client and the server
        self.test = test_pb2_grpc.TestServiceStub(self.channel)
        self.user = user_pb2_grpc.UserServiceStub(self.channel)

    def get_server_response(self, message):
        response = test_pb2.MessageRequest(message=message)
        return self.test.GetServerResponseMicro(response)

    def get_server_response_2(self, message):
        # AttributeError: 'TestServiceStub' object has no attribute 'GetServerResponseTwoMicro'
        response = test_pb2.MessageRequest(message=message)
        # print(self.test.__dir__())
        # ['GetServerResponseMicro', 'GetServerResponseTwoMicro', '__module__', '__doc__', '__init__', '__dict__', '__weakref__', '__repr__', '__hash__', '__str__', '__getattribute__', '__setattr__', '__delattr__', '__lt__', '__le__', '__eq__', '__ne__', '__gt__', '__ge__', '__new__', '__reduce_ex__', '__reduce__', '__subclasshook__', '__init_subclass__', '__format__', '__sizeof__', '__dir__', '__class__']
        return self.test.GetServerResponseTwoMicro(response)
        # return "5555"

    def get_user(self, id="1000"):
        response = base_pb2.ById(id=id)
        return self.user.GetUserMicro(response)
    

if __name__ == "__main__":
    compile_protos()
    test_client = TestClient()
    
    result = test_client.get_server_response(message="test message...")
    print(f"result: {result}")
    
    user = test_client.get_user(id=random.randint(1, 2000).__str__())
    print(f"user: {user}")

    result2 = test_client.get_server_response_2(message="[1,3,5,7,9]")
    print(f"result 2: {result2} ")
