import time
from concurrent import futures

import config
import grpc
import test_pb2
import test_pb2_grpc
import user_pb2
import user_pb2_grpc
# import question_generation_pb2
# import question_generation_pb2_grpc

from compile_grpc import compile_protos


class UserService(user_pb2_grpc.UserService):
    def __init__(self, *args, **kwargs):
        pass

    def GetUserMicro(self, request, target):
        id = request.id
        print(f"[Python] recv id from client: {request.id}")
        response = {
            "statusCode": 200,
            "message": f"get user micro: {id}",
            "error": "test",
            "data": "test",
        }
        return user_pb2.ResponseUser(**response)


class TestService(test_pb2_grpc.TestService):
    def __init__(self, *args, **kwargs):
        ...

    def GetServerResponseMicro(self, request, context):
        print(f"[Python] message from client: {request.message}")
        result = {"message": f"[{request.message}]", "received": True}
        return test_pb2.MessageResponse(**result)

    def GetServerResponseTwoMicro(self, request, context):
        print(f"[Python] message from client: {request.message}")
        # assuming input is a list of numbers [1,2,3]
        cleaned_message = request.message.replace("[","").replace("]","").split(",")
        output = str(sum([ float(s.strip()) for s in cleaned_message]))
        result = {"message": output, "received": True, "meta": None}
        return test_pb2.MessageResponse(**result)

# class QuestionGenerationService(question_generation_pb2_grpc.QuestionGenrationService):
#     def __init__(self, *args, **kwargs) -> None:
#         pass

#     def GenerateQuestionsMicro(self, request:question_generation_pb2.InputText, context):
#         return question_generation_pb2.OutputText({"text": request.text + "I generated sometthing here."})


def serve():
    print("on serve...")

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    test_pb2_grpc.add_TestServiceServicer_to_server(TestService(), server)
    user_pb2_grpc.add_UserServiceServicer_to_server(UserService(), server)

    server.add_insecure_port(f"[::]:{config.MICRO_TEST_PORT}")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    print(f"server.py... {config.MICRO_TEST_HOST}:{config.MICRO_TEST_PORT}")
    serve()
